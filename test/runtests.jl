using XmlTestReport

@testset "test xml report" begin
    run(pipeline(`julia --project='.' -q -e 'include("_runtests.jl")'`, devnull, devnull))

    truth = [
        "<?xml version=\"1.0\" encoding=\"utf-8\"?>",
        "<testsuite name=\"tests\">",
        "<testcase classname=\"_runtests\" file=\"_runtests.jl\" line=\"7\" name=\"line_7\" time=\"0\"></testcase>",
        "<testcase classname=\"_runtests\" file=\"_runtests.jl\" line=\"8\" name=\"line_8\" time=\"0\"><failure message=\"Test Failed at /home/pol/assemble/XmlTestReport/test/_runtests.jl:8",
        "  Expression: x == 2",
        "   Evaluated: 1 == 2\"</failure></testcase>",
        "<testsuite name=\"passes\" errors=\"0\" failures=\"0\" skipped=\"0\" tests=\"2\" time=\"0\">",
        "<testcase classname=\"_runtests\" file=\"_runtests.jl\" line=\"13\" name=\"line_13\" time=\"0\"></testcase>",
        "<testcase classname=\"_runtests\" file=\"_runtests.jl\" line=\"14\" name=\"line_14\" time=\"0\"></testcase>",
        "</testsuite>",
        "<testsuite name=\"doesn't pass\" errors=\"0\" failures=\"1\" skipped=\"0\" tests=\"2\" time=\"0\">",
        "<testcase classname=\"_runtests\" file=\"_runtests.jl\" line=\"19\" name=\"line_19\" time=\"0\"></testcase>",
        "<testcase classname=\"_runtests\" file=\"_runtests.jl\" line=\"20\" name=\"line_20\" time=\"0\"><failure message=\"Test Failed at /home/pol/assemble/XmlTestReport/test/_runtests.jl:20",
        "  Expression: x + 1 == 3",
        "   Evaluated: 2 == 3\"</failure></testcase>",
        "</testsuite>",
        "</testsuite>",
    ]

    outp = readlines(open("/tmp/tmptest.xml", "r"))
    @test length(outp) == length(truth)
    for i = zip(truth, outp)
        if length(i[1]) > 120
            @test strip(i[1])[1:120] == strip(i[2])[1:120]
        else
            @test strip(i[1]) == strip(i[2])
        end
    end
end
