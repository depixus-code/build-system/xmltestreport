using XmlTestReport
using XmlTestReport: FallbackTestSetException, TestSetException

@test_toxml begin
    x = 1
    try
        @test x == 1
        @test x == 2
    catch FallbackTestSetException
    end

    @testset "passes" begin
      @test x == 1
      @test (x+1) == 2
    end

    try
        @testset "doesn't pass" begin
          @test x == 1
          @test (x+1) == 3
        end
    catch TestSetException
    end
end "/tmp/tmptest.xml"
